'use strict';

/////////////////////////////////////////////////////
// plugins
//////////////////////////

// common
var gulp = require('gulp');
var extname = require('gulp-extname');
var gulpif = require('gulp-if');

// sass
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');

// js
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');

// html
var assemble = require('assemble');
var htmlmin = require('gulp-htmlmin');
var app = assemble();

/////////////////////////////////////////////////////
// CSS
//////////////////////////
var taskSass = function (production) {
    return gulp.src('./src/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulpif(production, minifyCss()))
        .pipe(gulp.dest('./public/css'));
};
gulp.task('sass', function () { taskSass(false); });
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// JS
//////////////////////////
var taskBrowserify = function (production) {
    browserify('./src/js/main.es6')
        .transform(babelify, {presets: ['es2015']})
        .bundle()
        .on("error", function (err) { console.log("Error : " + err.message); })
        .pipe(source('main.js'))
        .pipe(buffer())
        .pipe(gulpif(production, uglify()))
        .pipe(gulp.dest('./public/js'));
};
gulp.task('browserify', function () { taskBrowserify(false); });
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// HTML
//////////////////////////
gulp.task('load-hbs', function (cb) {
    app.partials('./src/template/partials/*.hbs');
    app.layouts('./src/template/layouts/*.hbs');
    app.pages('./src/template/pages/*.hbs');
    cb();
});
var taskAssemble = function (production) {
    return app.toStream('pages')
        .pipe(app.renderFile())
        .pipe(gulpif(production, htmlmin({ collapseWhitespace: true })))
        .pipe(extname())
        .pipe(app.dest('./public'));
};
gulp.task('assemble', ['load-hbs'], function () { taskAssemble(false); });
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// watch
//////////////////////////
gulp.task('watch', function () {
    gulp.watch('./src/template/**/*.hbs', ['assemble']);
    gulp.watch('./src/scss/*.scss', ['sass']);
    gulp.watch('./src/js/*.es6', ['browserify']);
});
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
// register tasks
//////////////////////////
gulp.task('default', ['sass', 'browserify', 'assemble', 'watch']);
gulp.task('prod', function () {
    taskSass(true);
    taskBrowserify(true);
    taskAssemble(true);
});
/////////////////////////////////////////////////////
