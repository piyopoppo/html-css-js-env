# html-css-js-env
This is useful environment for HTML/CSS/JS site developers.

## License

These codes are licensed under CC0.

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/deed.ja)

## Features

### HTML
Support Assemble (using Handlebars).

### CSS
Support Sass.

### JavaScript
Support ECMAScript 6(convert ES5 automatically).

## How to use

### Basic
* Install node.js and npm.
* npm i
* gulp

### For production
* gulp prod

